package com.example

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.Props
import akka.testkit.{ TestActors, TestKit, ImplicitSender }
import io.scalac.training.akka.ping.CoordinatorActor.Terminate
import io.scalac.training.akka.ping.PingActor.PingPongRequest
import io.scalac.training.akka.ping.{PongActor, PingActor}
import org.scalatest.WordSpecLike
import org.scalatest.Matchers
import org.scalatest.BeforeAndAfterAll
 
class PingPongActorSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {
 
  def this() = this(ActorSystem("MySpec"))
 
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }
 
  "A Ping actor" should {
    "send back a ping on a pong" in {
      val pingActor = system.actorOf(PingActor.props(10))
      pingActor ! PongActor.PongMessage("pong")
      expectMsg(PingActor.PingMessage("ping"))
      expectNoMsg()
    }

    "send back Terminate after repeats reached" in {
      val pingActor = system.actorOf(PingActor.props(2))
      pingActor ! PingPongRequest
      pingActor ! PongActor.PongMessage("pong")
      expectMsg(PingActor.PingMessage("ping"))
      pingActor ! PongActor.PongMessage("pong")
      expectMsg(Terminate)
      expectNoMsg()
    }
  }

  "A Pong actor" should {
    "send back a pong on a ping" in {
      val pongActor = system.actorOf(PongActor.props)
      pongActor ! PingActor.PingMessage("ping")
      expectMsg(PongActor.PongMessage("pong"))
    }
  }

}
