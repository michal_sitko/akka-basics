package io.scalac.training.akka.processing

import akka.actor.Actor.Receive
import akka.actor._
import io.scalac.training.akka.processing.ClientActor.Initialize
import io.scalac.training.akka.processing.ProcessingCoordinatorActor.ClientFinished

object ProcessingMain {
  def main(args: Array[String]): Unit = {

    val system = ActorSystem("HelloSystem")


    val dataSourceActor = system.actorOf(DataSourceActor.props)
    val processorActor = system.actorOf(ProcessorActor.props(dataSourceActor))
    val clientActors = for (i <- 0 to 9) yield {
      system.actorOf(ClientActor.props(processorActor))
    }

    val coordinator = system.actorOf(ProcessingCoordinatorActor.props(clientActors.toList))

    system.awaitTermination()
  }
}

class ProcessingCoordinatorActor (clientActors: List[ActorRef]) extends Actor {

  var waitingFor = clientActors.size

  for(actor <- clientActors) {
    actor ! Initialize
  }

  override def receive: Receive = {
    case ClientFinished =>
      waitingFor -= 1
      if(waitingFor == 0){
        context.system.shutdown()
      }
  }
}

object ProcessingCoordinatorActor {
  def props(clientActors: List[ActorRef]): Props = {
    Props(new ProcessingCoordinatorActor(clientActors))
  }

  case object ClientFinished
}
