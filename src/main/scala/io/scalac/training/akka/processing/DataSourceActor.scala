package io.scalac.training.akka.processing

import akka.actor.{Props, Actor}
import io.scalac.training.akka.processing.ClientActor.ReceivedNumber

import util.Random
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global

class DataSourceActor extends Actor {
  val random = new Random(java.lang.System.currentTimeMillis())

  override def receive: Receive = {
    case n: Int =>
      val delay = random.nextInt(50) + 50
      context.system.scheduler.scheduleOnce(delay.millis, sender(), ReceivedNumber(n))
  }
}

object DataSourceActor {
  def props = Props(new DataSourceActor)
}
