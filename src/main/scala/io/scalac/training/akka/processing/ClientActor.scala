package io.scalac.training.akka.processing

import akka.actor.{ActorRef, Props, Actor}
import akka.actor.Actor.Receive
import io.scalac.training.akka.processing.ProcessingCoordinatorActor.ClientFinished

class ClientActor(processor: ActorRef) extends Actor {
  import ClientActor._
  
  var receivedNumbers = List.empty[Int]
  val repeats = 6
  var counter = 0
  var initializer: ActorRef = _

  override def receive: Receive = {
    case Initialize =>
      initializer = sender()
      processor ! counter

    case ReceivedNumber(number) =>
      receivedNumbers = number :: receivedNumbers
      counter += 1
      if (counter == repeats) {
        println(s"ClientActor result: $receivedNumbers")
        initializer ! ClientFinished
      } else {
        processor ! counter
      }
  }
}

object ClientActor {
  def props(processor: ActorRef) = Props(new ClientActor(processor))

  case object Initialize
  case class ReceivedNumber(number: Int)
}
