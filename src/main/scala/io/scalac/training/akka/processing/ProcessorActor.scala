package io.scalac.training.akka.processing

import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout

import concurrent.duration._

class ProcessorActor(dataSource: ActorRef) extends Actor {
  implicit val timeout = Timeout(3000.millis)

  /**
   * Exercise 1.3
   *
   * Change the implementation of receive so the sender
   * of the n: Int (clientActor) receives ReceivedNumber(process(n))
   * instead of just ReceivedNumber(n)
   *
   * Tip: use akka.pattern.ask
   */
  override def receive: Receive = {
    case n: Int =>
      dataSource forward n
  }

  private def process(n: Int) = n * n
}

object ProcessorActor {
  def props(dataSource: ActorRef) = Props(new ProcessorActor(dataSource))
}
