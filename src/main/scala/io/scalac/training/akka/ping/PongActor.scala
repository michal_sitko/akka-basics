package io.scalac.training.akka.ping

import akka.actor.{Actor, ActorLogging, Props}

/**
 * Exercise 1.2
 *
 * 1. Create PongActor class as actor.
 * 2. PongActor is supposed to handle one type of message: PingActor.PingMessage.
 * It should println/log received message and send back PongMessage to
 * sender.
 *
 * Tip: To get actorRef to actor that sent message use `sender()`
 *
 */
class PongActor

object PongActor {
  // use commented out code (??? used just to make it compile)
  val props = ??? // Props[PongActor]
  case class PongMessage(text: String)
}
