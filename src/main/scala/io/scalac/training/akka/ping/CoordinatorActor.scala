package io.scalac.training.akka.ping

import akka.actor.{Props, ActorLogging, Actor, ActorRef}

/**
 * Exercise 1.2
 *
 * This actor is responsible for 2 things:
 * 1. Starts the whole process of ping-ponging by
 * sending PingPongRequest to pingActor.
 * 2. call `context.system.shutdown` after receiving PingPongFinished
 *
 */
class CoordinatorActor(pingActor: ActorRef, repeats: Int) extends Actor with ActorLogging {
  import PingActor._

  override def receive: Receive = ???
}

object CoordinatorActor {
  def props(pingActor: ActorRef, repeats: Int) = Props(new CoordinatorActor(pingActor, repeats))

  case object PingPongFinished
}
