package io.scalac.training.akka.ping

import akka.actor._
import io.scalac.training.akka.ping.PingActor.PingPongRequest

import util.Try

object PingMain {

  type ErrorMessage = String

  def main(args: Array[String]): Unit = {
    parseArguments(args) match {
      case Right(repeats) =>
        runPingPong(repeats)
      case Left(errorMessage) =>
        println(s"${Console.RED}Cannot proceed. $errorMessage${Console.RESET}")
    }
  }

  private def runPingPong(repeats: Int): Unit = {
    val system = ActorSystem("PingSystem")

    val pongActor = system.actorOf(PongActor.props, "pongActor")
    val pingActor = system.actorOf(PingActor.props(pongActor), "pingActor")

    val coordinatorActor = system.actorOf(CoordinatorActor.props(pingActor, repeats), "coordinatorActor")
    coordinatorActor ! PingPongRequest

    // await termination
    // context.system.shutdown should be called inside CoordinatorActor
    system.awaitTermination()
  }

  private def parseArguments(args: Array[String]): Either[ErrorMessage, Int] = {
    args match {
      case Array(firstArg, _*) => Try(firstArg.toInt)
        .map(asInt => Right(asInt))
        .getOrElse(Left("First argument cannot be parsed as Int"))
      case _ =>
        Left("Not enough arguments")
    }
  }

}
