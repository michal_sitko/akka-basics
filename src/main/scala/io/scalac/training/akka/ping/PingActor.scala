package io.scalac.training.akka.ping

import akka.actor.{ActorRef, Actor, ActorLogging, Props}

/**
 * Exercise 1.2
 *
 * 1. Implement logic for PingPongRequest
 * 2. Implement logic for PongActor.PongMessage. Print or log messages received
 * PongMessages to be able to manually test.
 *
 * If you need to save some state than add neccessary fields to PingActor
 * class.
 *
 * Tip: To send message to actor use syntax `actorRef ! someMessage`
 * Tip: To get actorRef to actor that sent message use `sender()`
 *
 */
class PingActor(pongActor: ActorRef) extends Actor with ActorLogging {
  import PingActor._

  def receive = {
  	case PingPongRequest(requestedRepeats) =>
	    // put your logic here
      ???
  	case PongActor.PongMessage(text) =>
      // put your logic here
      ???
  }	
}

object PingActor {
  def props(pongActor: ActorRef) = Props(new PingActor(pongActor))

  case class PingPongRequest(repeats: Int)
  case class PingMessage(text: String)
  case object PingPongFinished
}