package io.scalac.training.akka.hello

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}

object HelloMain {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("HelloSystem")
    val helloActor = system.actorOf(HelloActor.props, "helloActor")
    helloActor ! "Hello World"

    system.awaitTermination()
  }
}
