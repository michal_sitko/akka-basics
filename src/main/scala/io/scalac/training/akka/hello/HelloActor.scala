package io.scalac.training.akka.hello

import akka.actor.Actor.Receive
import akka.actor.{ActorLogging, Actor, Props}

/**
 * Exercise 1.1
 *
 * It's a warm-up exercise.
 *
 * Mix in Actor and ActorLogging. The actor should log
 * all string messages on warn level.
 *
 */
class HelloActor {

}

object HelloActor {
  // use commented out code (??? used just to make it compile)
  val props = ??? // Props[HelloActor]
}
